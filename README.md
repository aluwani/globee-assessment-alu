# Project Title

Globee assessment

## Getting Started


### Prerequisites

Before you can run the project you need to have the following software on your machine.
* PHP
* Apache/nginx
* MySQL
* composer
* ngrok
* Telegram app

On mac you can use [Laravel Valet](https://laravel.com/docs/5.7/valet) to quickly get up and running.
On windows or linux, [Homestead](https://laravel.com/docs/5.7/homestead) should provide all the tools you need to get started.

### Installing


Clone the repository
```
git clone git@bitbucket.org:aluwani/globee-assessment-alu.git
```

Create a new app on Telegram app. Install [BotFather](https://telegram.me/botfather)

```/newbot```
Take note of the access token

Add the following key to your .env file
```
TELEGRAM_BOT_API_ACCESS_TOKEN=XXXXXXX:XXXXXXXXXXXXXXXXXXX
FRONTEND_URL=http://yourappsdomain.co
```

Install dependencies
```
composer install
```

Run database migrations to create the tables in the database.
```php
php artisan migrate
```


Run the application
```
php artisan serve -port 5000
```
You need to have ngrok to make the application publicly accessible to the telegram bot.
```
ngrok http 5000
```
Copy the https public URL provided by ngrok, then run the command to update the 
```
php artisan telegram:webhook https://xxxxxxx.ngrok.io
```
Finally test the app on Telegram. Send a command ```/start```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

In the project directory, run:
```phpunit```

## Deployment

Add additional notes about how to deploy this on a live system

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Aluwani Raswiswi** - *Initial work* 

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments
