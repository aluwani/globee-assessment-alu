<?php

namespace App\Console\Commands\Telegram;

use GuzzleHttp\Client;
use Telegram\Bot\Commands\Command;

class GetBTCEquivalentCommand extends Command
{
    /**
     * The name of the console command.
     *
     * @var string
     */
    protected $name = 'getBTCEquivalent';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get the Bitcoin exchange rate in your chosen currency';

    /**
     * Execute the console command.
     *
     * @inheritdoc
     */
    public function handle($arguments)
    {
        // Check if any arguments passed.
        if ($arguments === "") {
            $this->replyWithMessage(['text' => 'Please use format /getBTCEquivalent 30 USD']);
            return true;
        }

        $params = explode(' ', $arguments);

        $currency = 'USD';
        $amount = $params[0];

        if (count($params) === 2) {
            $amount = $params[0];
            $currency = $params[1];
        }

        $client = new Client();

        try {
            $response = $client->get('https://api.coindesk.com/v1/bpi/currentprice/' . $currency . '.json')->getBody()->getContents();
            $response = json_decode($response, true);
            $rate = $response['bpi'][$currency]['rate_float'];

            $value = $amount / $rate;

            $message = "{$amount} {$currency} is {$value} BTC ({$rate }USD - 1 BTC)";
            $this->replyWithMessage(['text' =>  $message]);

        } catch (\Exception $error) {
            $this->replyWithMessage(['text' => 'There is a problem with our exchange rates provider. Please try again later']);
        }
    }
}
