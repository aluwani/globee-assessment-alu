<?php

namespace App\Console\Commands\Telegram;

use App\User;
use Illuminate\Support\Facades\Hash;
use Telegram\Bot\Commands\Command;

class SetUserCommand extends Command
{
    /**
     * The name of the console command.
     *
     * @var string
     */
    protected $name = 'setuser';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Allows you to setup an account.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle($arguments)
    {
        if ($arguments === "") {
            $this->replyWithMessage(['text' => 'Please use format /setuser john@example.com yourpassword']);
            return true;
        }

        [$email, $password] = explode(" ", $arguments);

        $telegram_id = $this->update->getMessage()->getFrom()->getId();
        $name = $this->update->getMessage()->getFrom()->getFirstName() . " "
            . $this->update->getMessage()->getFrom()->getLastName();

        User::updateOrCreate(
            ['email' => $email],
            [
                'telegram_id' => $telegram_id,
                'name' => $name,
                'email' => $email,
                'password' => Hash::make($password)
            ]
        );

        $this->replyWithMessage(['text' => 'Thank you for registering your account.']);
    }
}
