<?php

namespace App\Console\Commands\Telegram;

use Telegram\Bot\Commands\Command;

class GetConfigUrl extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'config-url';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Returns a restricted frontend URL to configure CoinMania';

    /**
     * Execute the console command.
     *
     * @inheritdoc
     */
    public function handle($arguments)
    {
        $this->replyWithMessage(['text' => env('FRONTEND_URL')]);
    }
}
