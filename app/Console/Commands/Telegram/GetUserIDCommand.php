<?php

namespace App\Console\Commands\Telegram;

use App\User;
use Telegram\Bot\Commands\Command;

class GetUserIDCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'getUserID';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command returns the current user\'s ID';

    /**
     * Execute the console command.
     *
     * @inheritdoc
     */
    public function handle($arguments)
    {
        $telegramInfo = $this->update;
        $user = User::where('telegram_id', $telegramInfo->getMessage()->getFrom()->getId())->first();
        $this->replyWithMessage(['text' => $user->id]);
    }
}
