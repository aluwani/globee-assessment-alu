<?php

namespace App\Console\Commands\Telegram;

use Telegram\Bot\Commands\Command;

class StartCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get started command';

    /**
     * Execute the console command.
     *
     * @inheritdoc
     */
    public function handle($arguments)
    {
        $this->replyWithMessage(['text' => 'Welcome to Coin Mania. This app can get Bitcoin exchange rates for you. But first we need to create an account for you. :)']);

        $this->replyWithMessage(['text' => 'Please enter the following command to setup your username and password: /setuser john@example.com yourpassword']);
        // Create an account if the user is new

        dump($this->update);
    }
}
