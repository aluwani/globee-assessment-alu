<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Telegram\Bot\Api;
use Telegram\Bot\Exceptions\TelegramSDKException;

class SetTelegramWebhook extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'telegram:webhook 
                            { base_url : Webhook base url }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sets the telegram webhook for our bot.';

    /**
     * @var Api
     */
    private $telegramAPI;

    /**
     * Create a new command instance.
     *
     * @throws \Telegram\Bot\Exceptions\TelegramSDKException
     */
    public function __construct()
    {
        parent::__construct();
        $this->telegramAPI = new Api(env('TELEGRAM_BOT_API_ACCESS_TOKEN'));
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        try {
            $this->telegramAPI->setWebhook([
                'url' => $this->argument('base_url') . '/' . env('TELEGRAM_BOT_API_ACCESS_TOKEN') . '/' . 'webhook'
            ]);
            $this->info('Webhhook successfully updated.');
        } catch (TelegramSDKException $exception) {
            $this->error($exception->getMessage());
        }
    }
}
