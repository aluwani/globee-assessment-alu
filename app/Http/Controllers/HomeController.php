<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $telegram_user = true;
        dump(\Auth::user()->telegram_id);
        if (\Auth::user()->telegram_id === null) {
            $telegram_user = false;
            $telegram_warning = 'Account not linked with telegram. To link account please send following command in telegram app. /setuser john@example.com yourpassword';
        }

        return view('home', compact('telegram_user', 'telegram_warning'));
    }
}
