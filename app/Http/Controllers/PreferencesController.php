<?php

namespace App\Http\Controllers;

use App\Preference;
use Illuminate\Http\Request;

class PreferencesController extends Controller
{
    public function store(Request $request)
    {
        $currency = $request->only('currency');

        Preference::updateOrCreate(['user_id' => auth()->user()->id], [
            'user_id' => auth()->user()->id,
            $currency
        ]);

        return redirect()->back()->with('status', 'Successfully updated your preferences.');
    }
}
