@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if (!$telegram_user)
                <div class="alert alert-warning" role="alert">
                    {{ $telegram_warning }}
                </div>
            @else
                <div class="alert alert-success" role="alert">
                    Your telegram id is: {{ Auth::user()->telegram_id }}
                </div>
            @endif
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form action="/preferences" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="currency">Currency</label>
                            <select name="currency" id="currency" class="form-control">
                                <option value="usd">USD</option>
                                <option value="zar">ZAR</option>
                                <option value="cny">CNY</option>
                            </select>
                        </div>

                        <button class="btn btn-primary" type="submit">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
